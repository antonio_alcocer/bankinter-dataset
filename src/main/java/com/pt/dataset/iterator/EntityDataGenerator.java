package com.pt.dataset.iterator;
import java.util.ArrayList;
import java.util.List;

import org.databene.benerator.wrapper.IteratingGenerator;
import org.databene.commons.TypedIterable;
import org.databene.commons.iterator.ArrayIterable;


public class EntityDataGenerator extends IteratingGenerator<Long>  {
	public static Long publicCounter = 0L;
	
	public EntityDataGenerator(Long counter) {
		super();
		publicCounter = counter;
		
		List<Long> numColl = new ArrayList<Long>();
		
		for (long idx = 0; idx < counter; idx ++){
			numColl.add(idx);
		}
		
		TypedIterable<Long> iterable = new ArrayIterable<Long>(numColl.toArray(new Long[0]), Long.class);
		
		super.setIterable(iterable);
	}
	
}
