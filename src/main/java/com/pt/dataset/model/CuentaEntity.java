package com.pt.dataset.model;

import java.util.ArrayList;
import java.util.List;

import org.firebrandocm.dao.annotations.ColumnFamily;

@ColumnFamily
public class CuentaEntity {
	
	private String nif; 
	private String descEmisor;

	
	private List<Entity> associatedEntities = new ArrayList<Entity>();
	
	public CuentaEntity(String nif, String descEmisor) {
		super();
		this.nif = nif;
		this.descEmisor = descEmisor;
	}
	public List<Entity> getAssociatedEntities() {
		return associatedEntities;
	}
	public CuentaEntity() {
		super();
	}
	public String getNif() {
		return nif;
	}
	public void setNif(String nif) {
		this.nif = nif;
	}
	public String getDescEmisor() {
		return descEmisor;
	}
	public void setDescEmisor(String descEmisor) {
		this.descEmisor = descEmisor;
	}
	
}
