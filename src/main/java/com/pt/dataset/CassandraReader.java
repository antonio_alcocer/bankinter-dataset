package com.pt.dataset;
import static org.firebrandocm.dao.cql.QueryBuilder.*;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Random;

import org.apache.cassandra.thrift.ConsistencyLevel;
import org.firebrandocm.dao.Query;
import org.firebrandocm.dao.impl.hector.HectorPersistenceFactory;

import com.pt.dataset.model.CounterEntity;
import com.pt.dataset.model.CuentaEntity;
import com.pt.dataset.model.Entity;

public class CassandraReader {
	private String clusterName;
	private HectorPersistenceFactory factory;
	private int numCuentas;
	private int maxNumCuenta;
	private int blockSize;

	public CassandraReader() {
		super();
		
		List<Class<?>> entities = new ArrayList<Class<?>>();
		entities.add(Entity.class);
		entities.add(CounterEntity.class);
		entities.add(CuentaEntity.class);

		try {
			Properties props = new Properties();
			props.load(getClass().getClassLoader().getResourceAsStream("cassandra.properties"));
			clusterName = props.getProperty("cassandra.clustername");
			numCuentas = Integer.parseInt(props.getProperty("numCuentas"));
			maxNumCuenta = Integer.parseInt(props.getProperty("maxNumCuenta"));
			blockSize = Integer.parseInt(props.getProperty("blockSize"));
			
			factory = new HectorPersistenceFactory.Builder().
					defaultConsistencyLevel(ConsistencyLevel.valueOf(props.getProperty("cassandra.consistencylevel"))).
					clusterName(clusterName).
					defaultKeySpace(props.getProperty("cassandra.keyspace")).
					contactNodes(new String[] { props.getProperty("cassandra.rpc.addresses") }).
					thriftPort( Integer.parseInt(props.getProperty("cassandra.thrift.port") ) ).
					autoDiscoverHosts(Boolean.parseBoolean(props.getProperty("cassandra.autoDiscoverHosts") )).
					entities(entities).
					build();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void readBenchmark(){
		
		Random randomGenerator = new Random();
		
		DecimalFormat intFormat = new DecimalFormat("000000000000");
		
		List<String> ids = new ArrayList<String>();
		
		System.out.println("Generating random ids...");
		
		for(int idx = 0; idx < numCuentas; idx ++){
			int nextInt = randomGenerator.nextInt(maxNumCuenta);
			ids.add("0128"+intFormat.format(nextInt));
			
		}
		
		System.out.println("Executing query...");
		
		long t0 = System.currentTimeMillis();
		Query q = Query.get(select(allColumns(), from(CuentaEntity.class), where(keyIn(ids.toArray(new String[0])))));
		
		List<CuentaEntity> cuentaEntities = factory.getResultList(CuentaEntity.class, q);
		
		List<String> entityIds = new ArrayList<String>();
		
		for (CuentaEntity cuentaEntity : cuentaEntities) {
			//String[] associatedEntities = cuentaEntity.getAssociatedEntities().split(",");
			
			String[] associatedEntities = null;
			
			for (int i = 0; i < associatedEntities.length; i++) {
				String entityId = associatedEntities[i].split(":")[1];
				
				entityIds.add(entityId);
			}
		}
		
		List<Entity> entities = new ArrayList<Entity>();
		
		int size = entityIds.size();
		
		while (size > 0) {
			List<String> innerEntityIds = entityIds.subList(0, Math.min(blockSize, size));
			
			size -= innerEntityIds.size();
			
			Query query = Query.get(select(columns("key", "cuenta"), from("Entity"), where(keyIn(innerEntityIds.toArray(new String[0])))));
			entities.addAll(factory.getResultList(Entity.class, query));
		}
		long t1 = System.currentTimeMillis();
		
		System.out.println("Retrieved "+entities.size()+" results in "+(t1-t0)+" millis\n");
	}

	
	public static void main(String[] args) {
		CassandraReader reader = new CassandraReader();
		
		reader.readBenchmark();
		
	}
}
