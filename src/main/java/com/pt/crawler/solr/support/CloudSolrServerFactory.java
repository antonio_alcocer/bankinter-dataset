package com.pt.crawler.solr.support;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Properties;


import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.impl.CloudSolrServer;
import org.apache.solr.client.solrj.impl.HttpSolrServer;

public class CloudSolrServerFactory  {

	private String zkHosts;
	private String defaultCollection;
	private Integer timeout;
	private boolean noZk = false;
	private SolrServer solrServer;
	
	public CloudSolrServerFactory(){
		Properties props = new Properties();
		try {
			props.load(getClass().getClassLoader().getResourceAsStream(
					"cassandra.properties"));
			
			noZk = Boolean.parseBoolean(props.getProperty("solr.noZk"));
			zkHosts = props.getProperty("solr.url");
			defaultCollection = props.getProperty("solr.collection");

			initSolrServer();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private SolrServer initSolrServer() {
		if (noZk) {
			solrServer = new HttpSolrServer(zkHosts + "/" +defaultCollection);

			setSolrServer(solrServer);
			return solrServer;
		} else {

			CloudSolrServer cloudSolrServer;
			try {
				cloudSolrServer = new CloudSolrServer(zkHosts);
			} catch (MalformedURLException e) {
				throw new IllegalArgumentException(e);
			}

			if (timeout != null) {
				cloudSolrServer.setZkConnectTimeout(timeout);
				cloudSolrServer.setZkClientTimeout(timeout);
			}

			cloudSolrServer.setDefaultCollection(defaultCollection);
			this.setSolrServer(cloudSolrServer);
			return cloudSolrServer;
		}
	}

	public SolrServer getObject() {
		return getSolrServer();
	}

	public Integer getTimeout() {
		return timeout;
	}

	public void setTimeout(Integer timeout) {
		this.timeout = timeout;
	}

	public SolrServer getSolrServer() {
		return solrServer;
	}

	public void setSolrServer(SolrServer solrServer) {
		this.solrServer = solrServer;
	}
}
