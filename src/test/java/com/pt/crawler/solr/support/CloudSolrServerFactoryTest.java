package com.pt.crawler.solr.support;

import static org.junit.Assert.*;

import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.params.SolrParams;
import org.junit.BeforeClass;
import org.junit.Test;

public class CloudSolrServerFactoryTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Test
	public void testGetServer() throws SolrServerException {
		CloudSolrServerFactory factory = new CloudSolrServerFactory();
		
		SolrServer solrServer = factory.getSolrServer();
		
		SolrQuery query = new SolrQuery("*:*");
		
		QueryResponse response = solrServer.query(query);
		
		System.out.println(response.toString());
	}
}
